package com.jmi.backside.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmi.backside.backend.JobQueue;
import com.jmi.backside.model.Job;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class JobControllerTest {

    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JobQueue jobQueue;

    @Test
    void contextLoads() {
    }

    @Test
    void whenJobControllerInsertsJobThenOkStusAndTicketExpected() throws Exception {
        final String url = "/job";
        final Job job = new Job("uno", "A description");
        final Response response = new Response(1);

        this.mockMvc.
                perform(post(url).
                        contentType(MediaType.APPLICATION_JSON).
                        content(objectToJson(job))).
                        andExpect(status().isCreated()).
                        andExpect(content().json(objectToJson(response)));

        // Verify the job's been correctly added to the JobQueue.
        ArgumentCaptor<Job> argument = ArgumentCaptor.forClass(Job.class);
        verify(jobQueue, times(1)).put(argument.capture());

        assertEquals(job.getName(), argument.getValue().getName());
        assertEquals(job.getDescription(), argument.getValue().getDescription());
    }

    @Test
    void whenJobQueueRaisesExceptionThenControllerReturnsInternalError() throws Exception {
        final String url = "/job";
        final Job job = new Job("uno", "A description");

        doThrow(new InterruptedException()).when(jobQueue).put(any());

        this.mockMvc.
                perform(post(url).
                        contentType(MediaType.APPLICATION_JSON).
                        content(objectToJson(job))).
                andExpect(status().isInternalServerError());
    }

    private <T> String objectToJson(final T t) throws JsonProcessingException {
        return this.mapper.writeValueAsString(t);
    }
}