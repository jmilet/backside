package com.jmi.backside.model;

/**
 * It represents a simple Job. As project is created for demonstration purposes, a Job it's quite
 * a simple thing. It's just a 'name' and a 'description'.
 */
public class Job {
    private String name;
    private String description;

    public Job(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
