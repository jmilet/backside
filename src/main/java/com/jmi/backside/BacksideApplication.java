package com.jmi.backside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BacksideApplication {
	public static void main(String[] args) {
		SpringApplication.run(BacksideApplication.class, args);
	}
}
