package com.jmi.backside.rest;

/**
 * It models the response of the controller.
 */
public class Response {
    private int ticketId;

    public Response(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }
}
