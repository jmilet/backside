package com.jmi.backside.rest;

import com.jmi.backside.model.Job;
import com.jmi.backside.backend.JobQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * It receives a Job model via POST request and queues the job for background processing.
 * The fact of adding the job into the queue is enough to consider the item as created
 * http status code 201.
 *
 * It returns a 'Response' model which defines the ticketId assigned to the job. This value
 * is just a regular incremental counter. As it's accessed by concurrently by several instances
 * of the controller, it's been implemented as an AtomicInteger.
 */
@RestController
public class JobController {

    @Autowired
    private JobQueue jobQueue;

    private AtomicInteger ticketId = new AtomicInteger(0);

    @PostMapping("job")
    public ResponseEntity<Response> job(@RequestBody final Job job) {
        try {
            jobQueue.put(job);
            return new ResponseEntity<>(new Response(this.ticketId.addAndGet(1)), HttpStatus.CREATED);
        } catch (InterruptedException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
