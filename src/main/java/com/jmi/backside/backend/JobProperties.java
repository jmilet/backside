package com.jmi.backside.backend;

public class JobProperties {
    public final static String NUM_CONSUMERS_PROPERTY = "backend.jobconsumer.num";
    public final static String LOG_RESULT = "backend.jobconsumer.logresult";
}
