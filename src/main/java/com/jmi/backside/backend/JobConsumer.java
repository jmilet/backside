package com.jmi.backside.backend;

import com.jmi.backside.model.Job;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 * Implements a simple consumer just for demonstration purposes. It receives the jobQueue
 * to take jobs from.
 *
 * The implementation it's quite simple, just a simple Thread that logs the received job
 * model.
 */
public class JobConsumer extends Thread {
    private final int id;
    private static final Logger logger = LogManager.getLogger(JobConsumer.class);
    private JobQueue jobQueue;
    private boolean logResult;

    @Autowired
    private Environment env;

    public JobConsumer(final int id, final JobQueue jobQueue, final boolean logResult) {
        this.id = id;
        this.jobQueue = jobQueue;
        this.logResult = logResult;
    }

    @Override
    public void run() {
        logger.info(String.format("Starting thread %d", this.id));

        while (true) {
            final Job job = jobQueue.take();
            if (job == null) {
                    logger.info(String.format("Thread %d error taking element", id));
            }
            else {
                process(job, logResult);
            }
        }
    }

    public void process(final Job job, final boolean logResult) {
        if (logResult) {
            logger.info(String.format("Thread %d processe job with name %s and description %s", this.id, job.getName(), job.getDescription()));
        }
    }
}
