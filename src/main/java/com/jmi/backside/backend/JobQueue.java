package com.jmi.backside.backend;

import com.jmi.backside.model.Job;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * It models a regular blocking job queue, where to add and take jobs from. It runs
 * its own pool of consumers Threads. The number of consumers is configured in
 * the 'backend.jobconsumers.num' application's property.
 */
@Component
public class JobQueue {
    private static final BlockingQueue<Job> jobQueue = new ArrayBlockingQueue(100);
    private static final Logger logger = LogManager.getLogger(JobQueue.class);

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        final int numConsumers = Integer.parseInt(env.getProperty(JobProperties.NUM_CONSUMERS_PROPERTY));
        final boolean logResult = Boolean.parseBoolean(env.getProperty(JobProperties.LOG_RESULT));

        for(int i = 0; i < numConsumers; i++) {
            final Thread thread = new JobConsumer(i, this, logResult);
            thread.start();
        }
    }

    public void put(final Job job) throws InterruptedException {
        jobQueue.put(job);
    }

    public Job take() {
        try {
            return jobQueue.take();
        } catch (InterruptedException e) {
            logger.error(String.format("Interrupted while taking"));
            return null;
        }
    }
}
