# Backside Application

Backside is a simple Spring Boot job queue application that
pretends to show some concurrency aspects of the Java programming
language.

When it comes to concurrency there are several valid models. The
approach followed here is trying to mimic the one established in
the Go programming language, that is a model where a pool of
consumer goroutines (in this case threads) communicate through
channels (in this case) a synchronized blocking queue.

Please, notice that wer'e aware of other concurrent programming
models, like 'parallel streams'. They could be easily applied in
the logic of the consumer, but the simplicity of the problem to
solve (just to show the basic structure of the solution) doesn't
leave room for such goodies.

The important thing to consider here is that we decided to share
data via message passing instead of the more traditional locked
shared memory. Both types of communication have their pros and
cons. In our opinion the neatness of the design overcomes any
overhead of using a synchronized data structure would produce.
Please, watch this video https://www.youtube.com/watch?v=cN_DpYBzKso&t=174s
in order to get a better description of the pattern.

So our final design looks like:

```
                                      +--> Consumer 1
                                      |
                                      +--> Consumer 2
                                      |
curl -> REST controller -> Job Queue -|
  ^                                   |
  |                          |        |
  +--------- ticket ---------+        +--> Consumer 3
                                      |
                                      +--> Consumer N

```

Run it this way:

```
$ gradle build && gradle bootRun
```

Have fun!!!
